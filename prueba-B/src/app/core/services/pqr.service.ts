import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';

const API_URL = 'https://betenlacebackend.herokuapp.com';

@Injectable({
  providedIn: 'root'
})
export class PqrService {

  constructor(private http: HttpClient) { }

  public getIdClient(): Observable<any> {
    return this.http.get<any>(`${API_URL}/api/client/types_id`);
  }
  public getTypesTickets(): Observable<any> {
    return this.http.get<any>(`${API_URL}/api/client/types_tickets`);
  }

  public getAllTickets(): Observable<any> {
    return this.http.get<any>(`${API_URL}/api/ticket`);
  }
  public getTicket(id: string): Observable<any> {
    let headers = new HttpHeaders();
    headers.append('Content-Type', 'Application/json');
    headers.append('Accept', '/');
    return this.http.get<any>(`${API_URL}/api/ticket?id=${id}`, { headers: headers });
  }

  public deleteTicket(id: string): Observable<any> {
    const httpOptions = {
      headers: new HttpHeaders({ 'Content-Type': 'application/json' }),
      body: { id: [id]}
  };
    return this.http.delete(`${API_URL}/api/ticket`, httpOptions );
  }

  public deleteIdTickets(data: any): Observable<any> {
    const httpOptions = {
      headers: new HttpHeaders({ 'Content-Type': 'application/json' }),
      body: { id: data}
  };
    return this.http.delete(`${API_URL}/api/ticket/`, httpOptions);
  }

  public saveTicket(data: any): Observable<any> {
    return this.http.post(`${API_URL}/api/ticket`, data);
  }

}
