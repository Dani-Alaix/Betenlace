import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Ticket } from '../core/models/ticket';
import { PqrService } from '../core/services/pqr.service';

@Component({
  selector: 'app-help',
  templateUrl: './help.component.html',
  styleUrls: ['./help.component.scss']
})
export class HelpComponent implements OnInit {

  public tickets: Ticket[] = [];
  public filterTickets: Ticket[] = [];
  public currentTicket: Ticket | undefined;
  public groupTickets: any = [];
  public identification: any;

  public types: any;
  public new: boolean = false;
  public saving: boolean = false;

  public ticketForm: FormGroup;
  public titleControl: FormControl;
  public descriptionControl: FormControl;
  public typeTicketControl: FormControl;
  public nameControl: FormControl;
  public lastNameControl: FormControl;
  public typeDocument: FormControl;
  public numberDoc: FormControl;
  public cellphoneControl: FormControl;
  public phoneControl: FormControl;
  public emailControl: FormControl;

  constructor(private pqrService: PqrService) {
    this.titleControl = new FormControl('', Validators.required);
    this.descriptionControl = new FormControl('', Validators.required);
    this.typeTicketControl = new FormControl('', Validators.required);
    this.nameControl = new FormControl('', Validators.required);
    this.lastNameControl = new FormControl('', Validators.required);
    this.typeDocument = new FormControl('', Validators.required);
    this.numberDoc = new FormControl('', Validators.required);
    this.cellphoneControl = new FormControl('', Validators.required);
    this.phoneControl = new FormControl('');
    this.emailControl = new FormControl('', [Validators.required, Validators.email]);
    this.ticketForm = new FormGroup({
      titleControl: this.titleControl,
      descriptionControl: this.descriptionControl,
      typeTicketControl: this.typeTicketControl,
      nameControl: this.nameControl,
      lastNameControl: this.lastNameControl,
      typeDocument: this.typeDocument,
      numberDoc: this.numberDoc,
      cellphoneControl: this.cellphoneControl,
      phoneControl: this.phoneControl,
      emailControl: this.emailControl,
    });
  }

  ngOnInit() {
    this.allTickets();
    this.typesTicket();
    this.typeId();
    this.filterSelection('all');
  }


  allTickets() {
    this.pqrService.getAllTickets().subscribe(
      res => this.tickets = res.tickets,
      err => this.tickets = []
    );
  }

  typesTicket() {
    this.pqrService.getTypesTickets().subscribe(
      res => {
        let newArrayTypes = Object.values(res.ticket_types);
        this.types = newArrayTypes;
      },
      err => this.types = ['Petición', 'Queja', 'Reclamo']
    );
  }

  filterSelection(type: string) {
    if (type === 'all' || type == '') {
      this.allTickets();
      this.filterTickets = this.tickets;
    }
    else {
      this.filterTickets = this.tickets.filter(e => e.type_ticket == type);
    }
  }


  typeId() {
    this.pqrService.getIdClient().subscribe(
      res => {
        let newArray = Object.values(res.identification_types);
        this.identification = newArray;
      },
      err => this.identification = ['Cédula de ciudadania', 'Cédula de extranjeria', 'Tarjeta de identidad']
    );
  }

  cancelTicket() {
    this.new = false;
    this.ticketForm.reset();
  }

  saveTicket() {
    if (this.ticketForm.valid) {
      this.saving = true;
      let data = {
        ni: this.numberDoc.value,
        type_doc: this.typeDocument.value,
        name: this.nameControl.value,
        last_name: this.lastNameControl.value,
        cellphone: this.cellphoneControl.value,
        phone: this.phoneControl.value,
        email: this.emailControl.value,
        title: this.titleControl.value,
        description: this.descriptionControl.value,
        type_ticket: this.typeTicketControl.value,
      }

      this.pqrService.saveTicket(data).subscribe(
        res => {
          this.saving = false;
          alert(data.type_ticket + res.id + ' enviado con exito.');
          this.ticketForm.reset();
          this.allTickets();
        }, err => {
          this.saving = false;
          alert('Ocurrió un error al guardar tu ' + data.type_ticket + ', intenta nuevamente.');
        }
      );
    } else {
      alert('Falta información, completa los datos antes de enviar.');
    }
  }

  ticketDetail(item: Ticket) {
    item.show = !item.show;
    if (item.show == true) {
      this.pqrService.getTicket(item.id.toString()).subscribe(
        res => this.currentTicket = res,
        err => { }
      );
    } else { }
  }

  delete(item: Ticket) {
    this.pqrService.deleteTicket(item.id.toString()).subscribe(
      res => this.filterSelection('all'),
      err => alert('Ocurrió un error al eliminar PQR, intenta nuevamente')
    );
  }

  onSelect(event: any) {
    if (!this.groupTickets.includes(event.target.value)) {
      this.groupTickets.push(event.target.value);
    }
    else if (this.groupTickets.includes(event.target.value)) {
      let i = this.groupTickets.indexOf(event.target.value);
      this.groupTickets.splice(i, 1);
    }
  }

  deleteGroup() {
    this.pqrService.deleteIdTickets(this.groupTickets).subscribe(
      res => console.log(res),
      err => console.log(err),
    );
  }

}
